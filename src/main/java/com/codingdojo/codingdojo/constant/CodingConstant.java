package com.codingdojo.codingdojo.constant;

public class CodingConstant {
    public static final String INGREDIENT_TYPE_FRUIT = "1-fru";
    public static final String INGREDIENT_TYPE_SPICE = "2-spi";
    public static final String INGREDIENT_TYPE_LIQUIDE = "3-liq";
    public static final String DRINK_TYPE_JUICE = "1-jui";
    public static final String DRINK_TYPE_BEER = "2-bee";
    public static final String DRINK_TYPE_COCKTAIL = "3-coc";
    public static final String FOOD_TYPE_CRISPS = "1-cri";
    public static final String FOOD_TYPE_SAUCISSON = "2-sau";
    public static final String FOOD_TYPE_CHEESE = "3-che";

}
