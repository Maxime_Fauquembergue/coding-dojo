package com.codingdojo.codingdojo.controller;

import com.codingdojo.codingdojo.controller.pojo.DrinkDTO;
import com.codingdojo.codingdojo.controller.pojo.FoodDTO;
import com.codingdojo.codingdojo.controller.pojo.IngredientDTO;
import com.codingdojo.codingdojo.service.DrinkService;
import com.codingdojo.codingdojo.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController()
public class CodingDojoController {

    @Autowired
    private DrinkService drinkService;

    @Autowired
    private FoodService foodService;

    @PostMapping("/myDemoEndpoint")
    @ResponseBody
    public String myDemoFindEndpoint(@RequestParam Optional<String> name) {
        return "Hello "+name.orElse("World");
    }

    @PostMapping("/findDrinkWithoutAlcohol")
    @ResponseBody
    public List<DrinkDTO> findDrinkWithoutAlcohol() {
        return drinkService.findDrinkWhithoutAlcohol();
    }

    @PostMapping("/findDrinkWithOptionalIngredient")
    @ResponseBody
    public List<DrinkDTO> findDrinkWithoutAlcohol(@RequestBody Optional<IngredientDTO> ingredient) {
        if (ingredient.isPresent()) {
            return drinkService.findDrinkWithOptionalIngredient(ingredient);
        } else {
            return drinkService.findDrinkWithOptionalIngredient(Optional.empty());
        }
    }

    @PostMapping("/findFoodOrderByPrice")
    @ResponseBody
    public List<FoodDTO> findFoodOrderByPrice() {
        return foodService.findFoodOrderByPrice();
    }

    @PostMapping("/getMostExpensiveFood")
    @ResponseBody
    public FoodDTO getMostExpensiveFood() {
        return foodService.getMostExpensiveFood();
    }

    @PostMapping("/getSumOfDrinkByType")
    @ResponseBody
    public Float getSumOfDrinkByType(@RequestParam Optional<String> optionalType) {
        return optionalType.isPresent()
                ? drinkService.getSumOfDrinkByType(optionalType)
                : drinkService.getSumOfDrinkByType(Optional.empty());
    }
}
