package com.codingdojo.codingdojo.controller;

import com.codingdojo.codingdojo.controller.pojo.DrinkDTO;
import com.codingdojo.codingdojo.controller.pojo.FoodDTO;
import com.codingdojo.codingdojo.controller.pojo.IngredientDTO;
import com.codingdojo.codingdojo.service.DrinkService;
import com.codingdojo.codingdojo.service.FoodService;
import com.codingdojo.codingdojo.service.MyDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController()
@RequestMapping("/correction")
public class MyDemoController {

    @Autowired
    private DrinkService drinkService;

    @Autowired
    private FoodService foodService;

    @Autowired
    private MyDemoService myDemoService;

    @PostMapping("/myDemoEndpoint")
    @ResponseBody
    public String myDemoFindEndpoint(@RequestParam Optional<String> name) {
        return "Hello "+name.orElse("World");
    }

    @PostMapping("/findDrinkWithoutAlcohol")
    @ResponseBody
    public List<DrinkDTO> findDrinkWithoutAlcohol() {
        return myDemoService.findDrinkWhithoutAlcohol();
    }

    @PostMapping("/findDrinkWithOptionalIngredient")
    @ResponseBody
    public List<DrinkDTO> findDrinkWithoutAlcohol(@RequestBody Optional<IngredientDTO> ingredient) {
        if (ingredient.isPresent()) {
            return myDemoService.findDrinkWithOptionalIngredient(ingredient);
        } else {
            return myDemoService.findDrinkWithOptionalIngredient(Optional.empty());
        }
    }

    @PostMapping("/findFoodOrderByPrice")
    @ResponseBody
    public List<FoodDTO> findFoodOrderByPrice() {
        return myDemoService.findFoodOrderByPrice();
    }

    @PostMapping("/getMostExpensiveFood")
    @ResponseBody
    public FoodDTO getMostExpensiveFood() {
        return myDemoService.getMostExpensiveFood();
    }

    @PostMapping("/getSumOfDrinkByType")
    @ResponseBody
    public Float getSumOfDrinkByType(@RequestParam Optional<String> optionalType) {
        return optionalType.isPresent()
                ? myDemoService.getSumOfDrinkByType(optionalType)
                : myDemoService.getSumOfDrinkByType(Optional.empty());
    }
}
