package com.codingdojo.codingdojo.controller.pojo;

import java.util.List;

public class DrinkDTO {

    private String drinkCode;
    private String name;
    private String type;
    private Boolean isWithAlcohol;
    private List<IngredientDTO> ingredientsList;
    private Float price;

    public DrinkDTO() {
    }

    public DrinkDTO(String drinkCode, String name, String type, Boolean isWithAlcohol,
                    List<IngredientDTO> ingredientsList,
                    Float price) {
        this.drinkCode = drinkCode;
        this.name = name;
        this.type = type;
        this.isWithAlcohol = isWithAlcohol;
        this.ingredientsList = ingredientsList;
        this.price = price;
    }

    public String getDrinkCode() {
        return drinkCode;
    }

    public void setDrinkCode(String drinkCode) {
        this.drinkCode = drinkCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getWithAlcohol() {
        return isWithAlcohol;
    }

    public void setWithAlcohol(Boolean withAlcohol) {
        isWithAlcohol = withAlcohol;
    }

    public List<IngredientDTO> getIngredientsList() {
        return ingredientsList;
    }

    public void setIngredientsList(List<IngredientDTO> ingredientsList) {
        this.ingredientsList = ingredientsList;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
