package com.codingdojo.codingdojo.controller.pojo;

import java.util.List;

public class FoodDTO {
    private String foodCode;
    private String name;
    private String type;
    private Float price;

    public FoodDTO() {
    }

    public FoodDTO(String foodCode, String name, String type, Float price) {
        this.foodCode = foodCode;
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public String getFoodCode() {
        return foodCode;
    }

    public void setFoodCode(String foodCode) {
        this.foodCode = foodCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
