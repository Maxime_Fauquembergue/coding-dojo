package com.codingdojo.codingdojo.controller.pojo;

public class IngredientDTO {

    private String ingredientCode;
    private String name;
    private String type;

    public IngredientDTO() {
    }

    public IngredientDTO(String ingredientCode, String name, String type) {
        this.ingredientCode = ingredientCode;
        this.name = name;
        this.type = type;
    }

    public String getIngredientCode() {
        return ingredientCode;
    }

    public void setIngredientCode(String ingredientCode) {
        this.ingredientCode = ingredientCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
