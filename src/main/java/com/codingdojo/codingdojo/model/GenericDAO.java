package com.codingdojo.codingdojo.model;

import com.codingdojo.codingdojo.constant.CodingConstant;
import com.codingdojo.codingdojo.controller.pojo.DrinkDTO;
import com.codingdojo.codingdojo.controller.pojo.FoodDTO;
import com.codingdojo.codingdojo.controller.pojo.IngredientDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GenericDAO {

    public List<DrinkDTO> findAllDrinks() {
        IngredientDTO orangeJuice = new IngredientDTO("orangeJuice", "Orange juice", CodingConstant.INGREDIENT_TYPE_LIQUIDE);
        IngredientDTO appleJuice = new IngredientDTO("appleJuice", "Apple juice",
                CodingConstant.INGREDIENT_TYPE_LIQUIDE);
        IngredientDTO tequila = new IngredientDTO("tequila", "Tequila",
                CodingConstant.INGREDIENT_TYPE_LIQUIDE);
        IngredientDTO pepper = new IngredientDTO("pepper", "Pepper", CodingConstant.INGREDIENT_TYPE_SPICE);
        IngredientDTO chilli = new IngredientDTO("chilli", "Chilli", CodingConstant.INGREDIENT_TYPE_SPICE);
        IngredientDTO ginger = new IngredientDTO("ginger", "Ginger", CodingConstant.INGREDIENT_TYPE_SPICE);
        IngredientDTO mango = new IngredientDTO("mango", "Mango", CodingConstant.INGREDIENT_TYPE_FRUIT);
        IngredientDTO pineapple = new IngredientDTO("pineapple", "Pineapple", CodingConstant.INGREDIENT_TYPE_FRUIT);

        DrinkDTO beerMangoGinger = new DrinkDTO("beerMangoGinger", "Ginger beer mango", CodingConstant.DRINK_TYPE_BEER,
                true,
                List.of(mango, ginger), new Float(9));
        DrinkDTO beerIPA = new DrinkDTO("IPA", "IPA beer", CodingConstant.DRINK_TYPE_BEER,
                true,
                null, new Float(7.5));
        DrinkDTO hothotCocktail = new DrinkDTO("hothot", "Hot Hot cocktail", CodingConstant.DRINK_TYPE_COCKTAIL,
                true,
                List.of(appleJuice, pepper, chilli, pineapple, tequila), new Float(8));
        DrinkDTO mocktail = new DrinkDTO("mocktail", "Mocktail", CodingConstant.DRINK_TYPE_COCKTAIL,
                false,
                List.of(orangeJuice, mango , pineapple, ginger), new Float(12));
        DrinkDTO orangeJuiceDrink = new DrinkDTO("orangeJuice", "Orange juice", CodingConstant.DRINK_TYPE_JUICE,
                false,
                List.of(orangeJuice), new Float(4));

        return List.of(beerMangoGinger, beerIPA, hothotCocktail, mocktail, orangeJuiceDrink);
    }

    public List<FoodDTO> findAllFoods(){
        FoodDTO normalCrisps = new FoodDTO("normalCrisps", "Normal Crisps", CodingConstant.FOOD_TYPE_CRISPS,
                new Float(1.5));
        FoodDTO vinegarCrisps = new FoodDTO("vinegarCrisps", "Vinegar Crisps", CodingConstant.FOOD_TYPE_CRISPS,
                new Float(1.5));
        FoodDTO bbqCrisps = new FoodDTO("bbqCrisps", "BBQ Crisps", CodingConstant.FOOD_TYPE_CRISPS,
                new Float(1.5));

        FoodDTO normalSaucisson = new FoodDTO("normalSaucisson", "Normal Saucisson", CodingConstant.FOOD_TYPE_SAUCISSON,
                new Float(5));
        FoodDTO cheeseSaucisson = new FoodDTO("cheeseSaucisson", "Cheese Saucisson", CodingConstant.FOOD_TYPE_SAUCISSON,
                new Float(6));
        FoodDTO herbsSaucisson = new FoodDTO("herbsSaucisson", "Herbs Saucisson", CodingConstant.FOOD_TYPE_SAUCISSON,
                new Float(6));

        FoodDTO camembert = new FoodDTO("camembert", "Camembert", CodingConstant.FOOD_TYPE_CHEESE,
                new Float(6));
        FoodDTO comte = new FoodDTO("comte", "Comte", CodingConstant.FOOD_TYPE_CHEESE,
                new Float(6));
        FoodDTO chevre = new FoodDTO("chevre", "Chevre", CodingConstant.FOOD_TYPE_CHEESE,
                new Float(6.5));

        return List.of(normalCrisps, vinegarCrisps, bbqCrisps, normalSaucisson, cheeseSaucisson, herbsSaucisson,
                camembert, comte, chevre);
    }
}
