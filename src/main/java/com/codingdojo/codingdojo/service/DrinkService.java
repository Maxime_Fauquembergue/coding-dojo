package com.codingdojo.codingdojo.service;

import com.codingdojo.codingdojo.controller.pojo.DrinkDTO;
import com.codingdojo.codingdojo.controller.pojo.IngredientDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface DrinkService {

    public List<DrinkDTO> findDrinkWhithoutAlcohol();

    public List<DrinkDTO> findDrinkWithOptionalIngredient(Optional<IngredientDTO> ingredientOfDrink);

    Float getSumOfDrinkByType(Optional<String> type);
}
