package com.codingdojo.codingdojo.service;

import com.codingdojo.codingdojo.controller.pojo.DrinkDTO;
import com.codingdojo.codingdojo.controller.pojo.IngredientDTO;
import com.codingdojo.codingdojo.model.GenericDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DrinkServiceImpl implements DrinkService {

    @Autowired
    private GenericDAO genericDAO;

    @Override
    public List<DrinkDTO> findDrinkWhithoutAlcohol() {
        return null;
    }

    @Override
    public List<DrinkDTO> findDrinkWithOptionalIngredient(Optional<IngredientDTO> ingredientOfDrink) {
        return null;
    }

    @Override
    public Float getSumOfDrinkByType(Optional<String> type) {
        return null;
    }
}
