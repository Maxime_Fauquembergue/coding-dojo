package com.codingdojo.codingdojo.service;

import com.codingdojo.codingdojo.controller.pojo.FoodDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FoodService {

    List<FoodDTO> findFoodOrderByPrice();

    FoodDTO getMostExpensiveFood();

}
