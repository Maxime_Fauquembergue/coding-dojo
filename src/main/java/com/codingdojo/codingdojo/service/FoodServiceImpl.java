package com.codingdojo.codingdojo.service;

import com.codingdojo.codingdojo.controller.pojo.FoodDTO;
import com.codingdojo.codingdojo.model.GenericDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodServiceImpl implements FoodService {

    @Autowired
    private GenericDAO genericDAO;

    @Override
    public List<FoodDTO> findFoodOrderByPrice() {
        return null;
    }

    @Override
    public FoodDTO getMostExpensiveFood() {
        return null;
    }
}
