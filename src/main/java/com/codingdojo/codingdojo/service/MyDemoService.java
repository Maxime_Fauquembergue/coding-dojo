package com.codingdojo.codingdojo.service;

import com.codingdojo.codingdojo.controller.pojo.DrinkDTO;
import com.codingdojo.codingdojo.controller.pojo.FoodDTO;
import com.codingdojo.codingdojo.controller.pojo.IngredientDTO;
import com.codingdojo.codingdojo.model.GenericDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

@Service
public class MyDemoService {

    @Autowired
    private GenericDAO genericDAO;

    // findWithoutAlcoholDrink
    public List<DrinkDTO> findDrinkWhithoutAlcohol() {
        List<DrinkDTO> allDrinks = genericDAO.findAllDrinks();

        return allDrinks.stream().filter(drink -> !drink.getWithAlcohol()).collect(Collectors.toList());
    }

    // findDrink using Optional withAlcohol
    public List<DrinkDTO> findDrinkWithOptionalIngredient(Optional<IngredientDTO> ingredientOfDrink) {
        List<DrinkDTO> allDrinks = genericDAO.findAllDrinks();

        if (ingredientOfDrink.isPresent()) {
            List<DrinkDTO> drinkContainingIngredient = new ArrayList<>();
            for (DrinkDTO drink : allDrinks) {
                List<String> ingredientCodes = CollectionUtils.isNotEmpty(drink.getIngredientsList())
                        ? drink.getIngredientsList().stream().map(IngredientDTO::getIngredientCode).collect(Collectors.toList())
                        : new ArrayList<>();

                if(ingredientCodes.stream().anyMatch(ingredientCode -> ingredientCode.equals(ingredientOfDrink.get().getIngredientCode()))) {
                    drinkContainingIngredient.add(drink);
                }
            }
            return drinkContainingIngredient;
        }

        return null;
    }

    public List<FoodDTO> findFoodOrderByPrice() {
        List<FoodDTO> allFood = genericDAO.findAllFoods();

        return allFood.stream().sorted(Comparator.comparing(FoodDTO::getPrice)).collect(Collectors.toList());
    }

    public FoodDTO getMostExpensiveFood() {
        return
                genericDAO.findAllFoods().stream().reduce((foodA, foodB) -> foodA.getPrice() > foodB.getPrice() ?
                        foodA : foodB).orElse(null);
    }

    public Float getSumOfDrinkByType(Optional<String> type) {
        if(type.isPresent()) {
            return genericDAO.findAllDrinks().stream().filter(drink -> type.get().equals(drink.getType())).map(DrinkDTO::getPrice).reduce(Float::sum).orElse(null);
        }
        return genericDAO.findAllDrinks().stream().map(DrinkDTO::getPrice).reduce(Float::sum).orElse(null);

    }

}
